<?php 
/**
* 
*/
class UsersController extends BaseController {
	
	public function index(){
		$usuarios = User::all();
		return Response::json(compact('usuarios'),200);
	}

	public function store(){
		$usuario = User::create(Input::only(array('name', 'email', 'password', 'rif', 'cedula', 'dob', 'direccion', 'sexo')));
		$usuario->password = Hash::make($usuario->password);
		$usuario->save();

		$this->_correo($usuario);

		return Response::json(compact('usuario'),200);		
	}

	public function login(){
		$post['email'] = Input::get('email');
		$post['password'] = Input::get('password');
		$post['active'] = 1;

		$data['status'] = false;
		$data['message'] = 'Usuario no verificado';

		if(!Auth::check()){
			if (Auth::attempt($post, true)){
				$data['status'] = true;
				$data['message'] = 'Usuario Logueado';
				$data['usuario'] = Auth::user();
			}else{
				$data['status'] = false;
				$data['message'] = 'Usuario no verificado, puede ser un problema de credenciales o que su cuenta aun no está activa';
			}
		}else{
			$data['status'] = true;
			$data['message'] = 'Retorno de usuario';
			$data['usuario'] = Auth::user();
		};
		return Response::json(compact('data'), 200);
	}

	public function logout(){
		Auth::logout();
		Session::flush();
		echo 'logueado pa fuera';
	}

	public function welcome(){
		$user = array(
			'email'=>'israel.simmons@gmail.com',
			'name'=>'Israel Simmons'
		);

		$encri['token'] = Crypt::encrypt($user['email']);

		// the data that will be passed into the mail view blade template
		$data = array(
			'detail'=>'Your awesome detail here',
			'name'	=> $user['name'],
			'link'	=> route('confirmation', $encri),
		);

		// use Mail::send function to send email passing the data and using the $user variable in the closure
		Mail::send('emails.welcome', $data, function($message) use ($user)
		{
		  $message->from('snap@buybuyez.com', 'Snap Admin');
		  $message->to($user['email'], $user['name'])->subject('Bienvenida Snap!');
		});
	}

	private function _correo($user){

		$encri['token'] = Crypt::encrypt($user['email']);

		// the data that will be passed into the mail view blade template
		$data = array(
			'detail'=>'Your awesome detail here',
			'name'	=> $user['name'],
			'link'	=> route('confirmation', $encri),
		);

		// use Mail::send function to send email passing the data and using the $user variable in the closure
		Mail::send('emails.welcome', $data, function($message) use ($user)
		{
		  $message->from('snap@buybuyez.com', 'Snap Admin');
		  $message->to($user->email, $user->name)->subject('Bienvenida Snap!');
		});
	}

	public function confirm($token){
		$email = Crypt::decrypt($token);
		$anunciantes = array();

		$usuario = User::where('email', '=', $email)->get()->first();
		$user = array(
			'email'=>$usuario->email,
			'name'=>$usuario->name
		);
		$usuario->active = 1;
		$usuario->save();

		$anunciante = new stdclass();
		$anunciante->nombre = 'Anunciante 1';
		$anunciante->imagen = 'http://snapapi.buybuyez.com/images/anunciantes/publicidad.png';

		$anunciantes[] = $anunciante;

		$encri['token'] = Crypt::encrypt($user['email']);

		// the data that will be passed into the mail view blade template
		$data = compact('anunciantes');

		// use Mail::send function to send email passing the data and using the $user variable in the closure
		Mail::send('emails.active', $data, function($message) use ($user)
		{
		  $message->from('snap@buybuyez.com', 'Snap Admin');
		  $message->to($user['email'], $user['name'])->subject('Cuenta activa Snap!');
		});	

		return View::make('confirmation');
	}
}