<?php

class TiendasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function activacion(){
		$tienda = Tiendas::get()->first();
		return Response::json(compact('tienda'), 200);
	}

	public function productos($tiendaId){
		$sql="select p.codigo, p.producto as nombre, i.precio, p.descripcion, im.tasa, cast(i.precio + (i.precio*im.tasa/100) as decimal(12,2)) as precio_imp, case when impuesto_id=1 then '(E)' else '' end as es_exento from inventario i
				inner join productos p on i.producto_id = p.id 
				inner join impuestos im on im.id=p.impuesto_id
				where i.tienda_id=?";
		$res = DB::select(DB::raw($sql),array($tiendaId));
		$response = new stdClass();

		foreach ($res as $key => $value) {
			$response->{$value->codigo} = $value;
		}


		return Response::json($response,200);
	}

	public function producto($tiendaId, $codigo){
		$sql="select p.codigo, p.producto as nombre, i.precio,p.imagen,p.descripcion, p.descripcion, im.tasa, cast(i.precio + (i.precio*im.tasa/100) as decimal(12,2)) as precio_imp, case when impuesto_id=1 then '(E)' else '' end as es_exento from inventario i
				inner join productos p on i.producto_id = p.id 
				inner join impuestos im on im.id=p.impuesto_id
				where i.tienda_id=? and codigo=?";
		$res = DB::select(DB::raw($sql),array($tiendaId, $codigo));
		
		return Response::json($res[0],200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}