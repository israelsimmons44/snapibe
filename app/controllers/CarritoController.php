<?php 
class CarritoController extends BaseController{
	public function guardarCarrito(){
		$productos = Input::get('productos');
		$config = Input::get('config');		

		$carrito = new Carrito();
		$carrito->cedula = $config['cedula'];
		$carrito->tienda_id = 1;

		$carrito->save();
		$guardado = array();
		
		foreach ($productos as $key => $value) {
			$detalle = new CarritoDetalle();
			$detalle->codigo = $value['codigo'];
			$detalle->cantidad = $value['qty'];
			$detalle->precio = floatval($value['precio_imp']);
			$detalle->carrito_id = $carrito->id;
			$detalle->save();
			$guardado[$value['codigo']] = $detalle;
			unset($detalle);
		}
		
		$imagen = DNS1D::getBarcodePNG($carrito->id, "EAN13");

		$response = Response::make(compact('carrito', 'imagen'),200);
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
        $response->header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        $response->header('Content-type', 'text/html; charset=UTF-8');
        
        return $response;
		
	}
}