<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inicial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productos', function(Blueprint $table){
			$table->increments('id');
			$table->string('codigo',20);
			$table->string('producto', 80);
			$table->string('descripcion', 500);
			$table->text('imagen');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('tiendas', function(Blueprint $table){
			$table->increments('id');
			$table->string('nombre',20);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('inventario', function(Blueprint $table){
			$table->increments('id');
			$table->integer('tienda_id')->unsigned();
			$table->integer('producto_id')->unsigned();
			$table->integer('cantidad')->default(0);
			$table->decimal('precio',12,2)->default(0);
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('tienda_id')->references('id')->on('tiendas');
			$table->foreign('producto_id')->references('id')->on('productos');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventario');
		Schema::drop('productos');
		Schema::drop('tiendas');
	}

}
