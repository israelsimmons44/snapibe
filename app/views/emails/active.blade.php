<p>Gracias, tu cuenta ha sido activada. Desde ahora puedes disfrutar de una experiencia única de compras.</p>

<p>En caso de preguntas o sugerencias, no dudes en contactarnos al correo: soporte@buybuyez.com</p>

<p>Saludos.</p>

<p>Visita a nuestros sponsors!!!</p>

@foreach ($anunciantes as $anunciante)
    <img src="{{$anunciante->imagen}}">
@endforeach