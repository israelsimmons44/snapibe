<?php 
class CarritoDetalle extends Eloquent{
	protected $table = 'carrito_detalle';
	protected $fillable = array('producto_id', 'cantidad', 'precio', 'carrito_id');
}