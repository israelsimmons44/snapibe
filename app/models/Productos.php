<?php 
class Productos extends Eloquent{
	protected $table = 'productos';
	protected $fillable = array('producto', 'codigo', 'imagen');
}