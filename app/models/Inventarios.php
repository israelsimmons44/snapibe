<?php 
class Inventarios extends Eloquent {
	protected $table = 'inventario';
	protected $fillable = array('tienda_id', 'producto_id', 'cantidad', 'precio');
}