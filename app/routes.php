<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::controller('password', 'RemindersController');

Route::group(array('prefix'=>'/api/v1'), function(){
	Route::get('activacion', 'TiendasController@activacion');
	Route::resource('productos', 'ProductosController');
	Route::resource('listas', 'ListaController');
	Route::get('tienda/{tiendaId}/producto/{productoId}', function($tiendaId, $productoId){
		$sql="select i.id, p.codigo, p.producto, i.precio  from inventario i
				inner join productos p on i.producto_id=p.id
				where tienda_id=? and p.codigo=?";

		$producto = DB::select(DB::raw($sql), array($tiendaId, $productoId))[0];

		return Response::json(compact('producto'),200);
	});
	Route::get('productosT/{tiendaId}', 'TiendasController@productos');
	Route::get('producto/{tiendaId}/{codigo}', 'TiendasController@producto');

	Route::post('guardarCarrito', 'CarritoController@guardarCarrito');
	Route::post('login', 'UsersController@login');
	Route::post('logout', 'UsersController@logout');
	Route::get('welcome', 'UsersController@welcome');
	Route::get('confirmation/{token}', array('as'=>'confirmation', 'uses'=> 'UsersController@confirm'));
	Route::resource('usuarios', 'UsersController');
	Route::get('barcode/{code}.svg', function($code){
		echo \DNS1D::getBarcodePNG($code, "EAN13");
	});
});
